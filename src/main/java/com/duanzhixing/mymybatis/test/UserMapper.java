package com.duanzhixing.mymybatis.test;

/**
 * @author duanzhixing
 *
 */
public interface UserMapper
{

    public User selectUser(int id);
}
