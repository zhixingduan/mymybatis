package com.duanzhixing.mymybatis.binding;

/**
 * @author duanzhixing
 * 
 * 解析我们的sql
 * @param <T>
 *用于存放sql语句和返回值类型的类
 */
public class MapperMethod<T>
{

    //sql语句
    private String sql;
    //返回的类
    private Class<T> type;

    public MapperMethod(String sql, Class<T> type)
    {
        super();
        this.sql = sql;
        this.type = type;
    }

    public MapperMethod()
    {
        super();
    }

    public String getSql()
    {
        return sql;
    }

    public void setSql(String sql)
    {
        this.sql = sql;
    }

    public Class<T> getType()
    {
        return type;
    }

    public void setType(Class<T> type)
    {
        this.type = type;
    }

}
