package com.duanzhixing.mymybatis.binding;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

import com.duanzhixing.mymybatis.session.DefaultSqlSession;

/**
 * @author duanzhixing
 *
 * @param <T>
 */
public class MapperProxy<T> implements InvocationHandler
{

    private final DefaultSqlSession sqlSession;
    private final Class<T> mapperInterface;

    public MapperProxy(DefaultSqlSession sqlSession, Class<T> mapperInterface)
    {
        this.sqlSession = sqlSession;
        this.mapperInterface = mapperInterface;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable
    {
        //获取Configuration中的MapperRegistry注册中心 通过id到KnownMappers中对于的mapperMethod对象
        MapperMethod mapperMethod = sqlSession.getConfiguration().getMapperRegistry().getKnownMappers().get(method.getDeclaringClass().getName() + "." + method.getName());
        if (null != mapperMethod)
        {
            //调用selectOne方法把mapperMethod传进去和需要的参数
            return sqlSession.selectOne(mapperMethod, Integer.valueOf(String.valueOf(args[0])));
        }
        return method.invoke(proxy, args);
    }
}
