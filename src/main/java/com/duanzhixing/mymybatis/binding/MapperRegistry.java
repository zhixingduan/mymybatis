package com.duanzhixing.mymybatis.binding;

import java.util.HashMap;
import java.util.Map;

/**
 * @author duanzhixing
 *
 * 配置我们相关的注册中心
 */
public class MapperRegistry
{

    /**
     * key是 xml中的 namespace+id+resultType
     * value是 MapperMethod对象 存放着sql：sql语句 type：返回值的class
     */
    private Map<String, MapperMethod> knownMappers = new HashMap<String, MapperMethod>();

    public Map<String, MapperMethod> getKnownMappers()
    {
        return knownMappers;
    }

    public void setKnownMappers(Map<String, MapperMethod> knownMappers)
    {
        this.knownMappers = knownMappers;
    }

}
