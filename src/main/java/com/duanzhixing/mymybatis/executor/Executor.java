package com.duanzhixing.mymybatis.executor;

import com.duanzhixing.mymybatis.binding.MapperMethod;

public interface Executor
{

    <T> T query(MapperMethod method, Object parameter) throws Exception;
}
