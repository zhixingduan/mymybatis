package com.duanzhixing.mymybatis.executor;

import java.sql.Connection;
import java.sql.PreparedStatement;

import com.duanzhixing.mymybatis.binding.MapperMethod;
import com.duanzhixing.mymybatis.result.DefaultResultSetHandler;
import com.duanzhixing.mymybatis.session.Configuration;
import com.duanzhixing.mymybatis.util.DbUtil;

/**
 * @author duanzhixing
 *
 */
public class SimpleExecutor implements Executor
{
    private Configuration configuration;

    private DefaultResultSetHandler resultSetHandler;

    public SimpleExecutor(Configuration configuration)
    {
        this.configuration = configuration;

        resultSetHandler = new DefaultResultSetHandler();
    }

    @Override
    public <T> T query(MapperMethod method, Object parameter) throws Exception
    {

        //使用工具类打开数据库连接
        Connection connection = DbUtil.open();
        //传入sql 和 值
        PreparedStatement preparedStatement = connection.prepareStatement(String.format(method.getSql(), Integer.parseInt(String.valueOf(parameter))));
        //执行执行器

        preparedStatement.execute();

        //把结果映射到对象中
        return resultSetHandler.handle(preparedStatement, method);

    }

}
