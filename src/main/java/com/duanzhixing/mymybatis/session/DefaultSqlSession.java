package com.duanzhixing.mymybatis.session;

import java.lang.reflect.Proxy;

import com.duanzhixing.mymybatis.binding.MapperMethod;
import com.duanzhixing.mymybatis.binding.MapperProxy;
import com.duanzhixing.mymybatis.executor.Executor;
import com.duanzhixing.mymybatis.executor.SimpleExecutor;

/**
 * @author duanzhixing
 * 一个简单的默认的sqlsession, 该类可以通过反射的方式获取我们Mapper。java的接口
 *
 */
public class DefaultSqlSession implements SqlSession
{
    //加载好的配置文件
    private Configuration configuration;
    //执行器
    private Executor executor;

    //使用动态代理 代理给MapperProxy
    public <T> T getMapper(Class<T> type)
    {
        return (T) Proxy.newProxyInstance(type.getClassLoader(), new Class[] { type }, new MapperProxy<>(this, type));
    }

    public DefaultSqlSession(Configuration configuration, SimpleExecutor simpleExecutor)
    {
        this.executor = simpleExecutor;
        this.configuration = configuration;
    }

    @Override
    public <T> T selectOne(MapperMethod mapperMethod, int valueOf) throws Exception
    {
        return executor.query(mapperMethod, (Object) valueOf);
    }

    public Configuration getConfiguration()
    {
        return configuration;
    }

    public void setConfiguration(Configuration configuration)
    {
        this.configuration = configuration;
    }

    public Executor getExecutor()
    {
        return executor;
    }

    public void setExecutor(Executor executor)
    {
        this.executor = executor;
    }

}
