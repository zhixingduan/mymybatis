package com.duanzhixing.mymybatis.session;

import com.duanzhixing.mymybatis.binding.MapperMethod;

/**
 * @author duanzhixing
 *
 */
public interface SqlSession
{

    public <T> T selectOne(MapperMethod mapperMethod, int id) throws Exception;

}
