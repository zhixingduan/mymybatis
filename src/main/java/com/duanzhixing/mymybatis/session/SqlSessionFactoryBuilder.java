package com.duanzhixing.mymybatis.session;

import java.io.IOException;

/**
 * @author duanzhixing
 * 用于返回我们的SqlSessionFactory对象
 */
public class SqlSessionFactoryBuilder
{
    //加载配置文件 返回SqlSessionFactory对象
    public SqlsessionFactory build(Configuration configuration) throws IOException
    {
        //读取xml
        configuration.loadConfigurations();
        return new SqlsessionFactory();
    }
}
