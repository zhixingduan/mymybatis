package com.duanzhixing.mymybatis.session;

import com.duanzhixing.mymybatis.executor.SimpleExecutor;

/**
 * @author duanzhixing
 *
 *sqlsession工厂
 */
public class SqlsessionFactory
{

    //把配置文件和执行器传入 返回SqlSession
    public SqlSession openSession(Configuration configuration)
    {
        return new DefaultSqlSession(configuration, new SimpleExecutor(configuration));
    }

}
