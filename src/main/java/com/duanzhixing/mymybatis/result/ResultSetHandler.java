package com.duanzhixing.mymybatis.result;

import java.sql.PreparedStatement;

import com.duanzhixing.mymybatis.binding.MapperMethod;

/**
 * @author duanzhixing
 *
 */
public interface ResultSetHandler
{
    public <T> T handle(PreparedStatement pstmt, MapperMethod mapperMethod) throws Exception;
}
