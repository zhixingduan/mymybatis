package com.duanzhixing.mymybatis;

import java.io.IOException;
import java.io.InputStream;

import org.junit.Test;

import com.duanzhixing.mymybatis.session.Configuration;
import com.duanzhixing.mymybatis.session.DefaultSqlSession;
import com.duanzhixing.mymybatis.session.SqlSessionFactoryBuilder;
import com.duanzhixing.mymybatis.session.SqlsessionFactory;
import com.duanzhixing.mymybatis.test.User;
import com.duanzhixing.mymybatis.test.UserMapper;

public class TestMybatis
{

    @Test
    public void test() throws IOException
    {

        InputStream inputStream = TestMybatis.class.getClassLoader().getResourceAsStream("mybatis.xml");
        Configuration configuration = new Configuration();
        //把流传入到Configuration对象中
        configuration.setInputStream(inputStream);
        //把配置文件读取配置文件返回sqlSessionFactory
        SqlsessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(configuration);
        //开启事务，获取加载器，返回sqlSession
        DefaultSqlSession sqlSession = (DefaultSqlSession) sqlSessionFactory.openSession(configuration);
        //调用加载器
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        User user = userMapper.selectUser(1);
        System.out.println(user);

    }

}
